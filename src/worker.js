import * as faceapi from 'face-api.js'
import { ipcRenderer } from 'electron'
import * as fs from 'fs'

// configure face API
faceapi.env.monkeyPatch({
  Canvas: HTMLCanvasElement,
  Image: HTMLImageElement,
  ImageData: ImageData,
  Video: HTMLVideoElement,
  createCanvasElement: () => document.createElement('canvas'),
  createImageElement: () => document.createElement('img')
})

const SSD_MOBILENETV1 = 'ssd_mobilenetv1'
// const TINY_FACE_DETECTOR = 'tiny_face_detector'

const selectedFaceDetector = SSD_MOBILENETV1

// ssd_mobilenetv1 options
const minConfidence = 0.5

// tiny_face_detector options
const inputSize = 512
const scoreThreshold = 0.5

function getFaceDetectorOptions () {
  return selectedFaceDetector === SSD_MOBILENETV1
    ? new faceapi.SsdMobilenetv1Options({ minConfidence })
    : new faceapi.TinyFaceDetectorOptions({ inputSize, scoreThreshold })
}

const loadNet = async () => {
  await faceapi.nets.ssdMobilenetv1.load('/static/weights')
  await faceapi.loadFaceLandmarkModel('/static/weights')
  await faceapi.loadFaceRecognitionModel('/static/weights')
}

function loadImage (url) {
  return new Promise((resolve, reject) => {
    const img = new Image()
    img.addEventListener('load', () => resolve(img))
    img.addEventListener('error', reject) // don't forget this one
    img.src = url
  })
}

let selfiesFolder = ''
let selfies = []

let searchFolder = ''
let images = []

const workerFuncs = {
  setSelfieFolder (folder) {
    selfiesFolder = folder
    // blah = err
    fs.readdir(folder, (blah, files) => {
      selfies = files.filter(fn => fn.toLowerCase().endsWith('.jpg'))
      reply('selfieFolderInfo', {
        selfiesCount: selfies.length
      })
      loadNet().then(workerFuncs.detectFace)
    })
  },

  setSearchFolder (folder) {
    searchFolder = folder
    fs.readdir(folder, (blah, files) => {
      images = files.filter(fn => fn.toLowerCase().endsWith('.jpg'))
      reply('searchFolderInfo', {
        imagesCount: images.length
      })
    })
  },

  async detectFace () {
    const refImage = document.getElementById('refImage')

    const foundImages = {}

    let img
    const descriptors = []

    for (let i = 0; i < 5; i++) {
      img = fs.readFileSync(`${selfiesFolder}/${selfies[i]}`).toString('base64')
      refImage.src = 'data:image/png;base64,' + img
      descriptors.push(await faceapi.computeFaceDescriptor(refImage))
    }

    const labeledFaceDescriptors = new faceapi.LabeledFaceDescriptors(
      'subin',
      descriptors
    )

    const faceMatcher = new faceapi.FaceMatcher(labeledFaceDescriptors)

    for (let i = 0; i < images.length; i++) {
      const queryImage = document.getElementById('queryImage')
      const queryCanvasP = queryImage.parentElement

      console.log(images[i])

      img = fs.readFileSync(`${searchFolder}/${images[i]}`).toString('base64')
      const url = 'data:image/png;base64,' + img

      loadImage(url).then(() => {
        const queryCanvas = queryCanvasP.getElementsByTagName('canvas')[0]

        const options = getFaceDetectorOptions()
        const results = await faceapi
          .detectAllFaces(queryImage, options)
          .withFaceLandmarks()
          .withFaceDescriptors()

        faceapi.matchDimensions(queryCanvas, queryImage)
        // resize detection and landmarks in case displayed image is smaller than
        // original size
        const resizedResults = faceapi.resizeResults(results, queryCanvas)

        resizedResults.forEach(({ detection, descriptor, landmarks }) => {
          const bestMatch = faceMatcher.findBestMatch(descriptor)

          if (bestMatch.label === 'subin') {
            console.log(1)
            foundImages[images[i]] = url

            reply('foundImage', {
              filename: images[i],
              url
            })
          }
        })
      })
    }
  }
}

const reply = (action, payload) => {
  ipcRenderer.send('message-from-worker', {
    action,
    payload
  })
}

ipcRenderer.on('message-from-window', (e, data) => {
  if (workerFuncs[data.action]) {
    workerFuncs[data.action].apply(this, data.payload)
  }
})
